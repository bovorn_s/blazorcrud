﻿using System;
namespace BlazorBookCrud.Pages
{
	public class BookService
	{
		private static List<Book> books = new List<Book>();

		public List<Book> GetBooks()
		{
			return books;
		}

		public void AddBook(Book book)
		{
			book.Id = books.Count + 1;
			books.Add(book);
		}

		public void UpdateBook(Book book)
		{
			var existingBook = books.Find(b => b.Id == book.Id);
			if (existingBook != null)
			{
				existingBook.Title = book.Title;
				existingBook.Author = book.Author;
			}
		}

		public void DeleteBook(int bookId)
		{
			books.RemoveAll(b => b.Id == bookId);
		}

        public static List<string> GetCategories()
        {
            return new List<string> { "Fiction", "Non-Fiction", "Science Fiction" };
        }

    }
}

